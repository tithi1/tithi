const NepMonths = [ 'Baisakh', 'Jeth', 'Asar', 'Saun', 'Bhadau', 'Ashoj', 'Kartik', 'Mangsir', 'Push', 'Magh', 'Fagun', 'Chait' ];

class NepYear {
    constructor(year, months) {
        if (months.length != 12)
            throw new Error(`Bad number of months ${months.length} for year ${year}`);
        this.year = year;
        this.months = months;
        this.total = 0;
        for (let i=0; i<this.months.length; i++)
            this.total+=this.months[i];
    };
    
    yearMonthAndDayFromOffset(offset) {
        if ((offset <=0) || (offset > this.total))
            return "";
        let i=0;
        while (offset > this.months[i]) {
            offset-=this.months[i];
            i+=1;
        }
        return `${NepMonths[i]} ${offset} ${this.year}`;
    }
};

// Year data based on https://github.com/Biplab-Dutta/Nepali_Date
const nepYears = [
    new NepYear(2074, [31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30]),
    new NepYear(2075, [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30]),
    new NepYear(2076, [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30]),
    new NepYear(2077, [31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31]),
    new NepYear(2078, [31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30]),
    new NepYear(2079, [31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30]),
    new NepYear(2080, [31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30]),
    new NepYear(2081, [31, 31, 32, 32, 31, 30, 30, 30, 29, 30, 30, 30]),
    new NepYear(2082, [30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30]),
    new NepYear(2083, [31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30]),
    new NepYear(2084, [31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30]),
    new NepYear(2085, [31, 32, 31, 32, 30, 31, 30, 30, 29, 30, 30, 30]),
    new NepYear(2086, [30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30]),
    new NepYear(2087, [31, 31, 32, 31, 31, 31, 30, 30, 29, 30, 30, 30]),
    new NepYear(2088, [30, 31, 32, 32, 30, 31, 30, 30, 29, 30, 30, 30]),
    new NepYear(2089, [30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30]),
    new NepYear(2090, [30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30])
]

const ref_date_nep = [2074, 1, 1];
const ref_date_eng = new Date(2017, 3, 14);

function getNepaliDate(inp_date_eng=new Date()) {
    let offset = 1 + Math.floor((inp_date_eng - ref_date_eng) / (1000 * 3600 * 24));

    let i = 0;
    while ((i < nepYears.length) && (offset > nepYears[i].total)) {
        offset -= nepYears[i].total;
        i++;
    }
    
    return nepYears[i].yearMonthAndDayFromOffset(offset);

}